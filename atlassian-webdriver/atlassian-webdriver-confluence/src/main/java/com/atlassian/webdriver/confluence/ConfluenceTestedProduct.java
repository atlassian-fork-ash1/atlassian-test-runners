package com.atlassian.webdriver.confluence;

import com.atlassian.pageobjects.Defaults;
import com.atlassian.pageobjects.MultiTenantTestedProduct;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.component.WebSudoBanner;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.pageobjects.page.AdminHomePage;
import com.atlassian.pageobjects.component.Header;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.pageobjects.page.WebSudoPage;
import com.atlassian.webdriver.AtlassianWebDriverModule;
import com.atlassian.webdriver.confluence.component.header.ConfluenceHeader;
import com.atlassian.webdriver.confluence.component.header.ConfluenceWebSudoBanner;
import com.atlassian.webdriver.confluence.page.ConfluenceAdminHomePage;
import com.atlassian.webdriver.confluence.page.ConfluenceLoginPage;
import com.atlassian.webdriver.confluence.page.ConfluenceWebSudoPage;
import com.atlassian.webdriver.confluence.page.DashboardPage;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import com.atlassian.webdriver.utils.BaseUrlSupplierForHost;

import java.util.Optional;

import static java.util.Objects.requireNonNull;


/**
 *
 */
@Defaults(instanceId = "confluence", contextPath = "/confluence", httpPort = 1990)
public class ConfluenceTestedProduct implements MultiTenantTestedProduct<WebDriverTester>
{
    private String loggedInUsername;
    private String loggedInPassword;
    private String host;

    private final PageBinder pageBinder;
    private final WebDriverTester webDriverTester;
    private final ProductInstance productInstance;

    public ConfluenceTestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance)
    {
        this.productInstance = requireNonNull(productInstance, "productInstance can't be null");;
        this.webDriverTester = testerFactory == null ? new DefaultWebDriverTester() : testerFactory.create();
        final BaseUrlSupplierForHost baseUrlSupplier = new BaseUrlSupplierForHost(productInstance.getBaseUrl(), () -> Optional.ofNullable(host));
        this.pageBinder = new InjectPageBinder(baseUrlSupplier, webDriverTester,
            new StandardModule(this), new AtlassianWebDriverModule(this), new ElementModule(), new TimeoutsModule());

        this.pageBinder.override(Header.class, ConfluenceHeader.class);
        this.pageBinder.override(HomePage.class, DashboardPage.class);
        this.pageBinder.override(LoginPage.class, ConfluenceLoginPage.class);
        this.pageBinder.override(AdminHomePage.class, ConfluenceAdminHomePage.class);
        this.pageBinder.override(WebSudoPage.class, ConfluenceWebSudoPage.class);
        this.pageBinder.override(WebSudoBanner.class, ConfluenceWebSudoBanner.class);
    }

    public DashboardPage gotoHomePage()
    {
        return pageBinder.navigateToAndBind(DashboardPage.class);
    }

    public ConfluenceAdminHomePage gotoAdminHomePage()
    {
        return pageBinder.navigateToAndBind(ConfluenceAdminHomePage.class);
    }

    public ConfluenceLoginPage gotoLoginPage()
    {
        return pageBinder.navigateToAndBind(ConfluenceLoginPage.class);
    }

    public <P extends Page> P visit(Class<P> pageClass, Object... args)
    {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    public PageBinder getPageBinder()
    {
        return pageBinder;
    }

    public ProductInstance getProductInstance()
    {
        return productInstance;
    }

    public WebDriverTester getTester()
    {
        return webDriverTester;
    }

    public String getLoggedInUsername() {
        return loggedInUsername;
    }

    public String getLoggedInPassword() {
        return loggedInPassword;
    }

    public void setLoggedInUser(String loggedInUsername, String password) {
        this.loggedInUsername = loggedInUsername;
        this.loggedInPassword = password;
    }

    @Override
    public void setHost(String host) {
        this.host = host;
    }

}
